package mx.hackathon.descubremisantla.fragments;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONObject;

import com.actionbarsherlock.app.SherlockFragment;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import mx.hackathon.descubremisantla.R;
import mx.hackathon.descubremisantla.activities.DetalleLugar;
import mx.hackathon.descubremisantla.activities.LugaresActivity.Lugar;
import mx.hackathon.descubremisantla.utils.Estaticos;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class MiUbicacionFragment extends SherlockFragment implements OnInfoWindowClickListener{
	
	public MiUbicacionFragment(){};
	private static GoogleMap mMap;
	private SupportMapFragment fragment;
	private ArrayList<Lugar> lugares_list;
	private HashMap<Marker, Lugar> list;
	private LatLng MISANTLA = new LatLng(19.928994, -96.853166);
	private int[] ICONS = {
		R.drawable.ic_pin_restaurant,
		R.drawable.ic_pin_parque,
		R.drawable.ic_pin_hotel,
		R.drawable.ic_pin_museo,
		R.drawable.ic_pin_iglesia,
		R.drawable.ic_pin_mercado,
		R.drawable.ic_pin_edificio,
	};
	
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		list = new HashMap<Marker,Lugar>();
		return inflater.inflate(R.layout.fragment_map,container, false);
    }
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		FragmentManager fm = getChildFragmentManager();
		fragment = (SupportMapFragment)fm.findFragmentById(R.id.location_map);
		if(fragment == null){
			fragment = SupportMapFragment.newInstance();
			fm.beginTransaction().replace(R.id.location_map,fragment).commit();
		}
	}
	  
	
	@Override
	public void onResume() {
		super.onResume();
		if(mMap == null){
			mMap = fragment.getMap();
			mMap.setMyLocationEnabled(true);
		    mMap.getUiSettings().setZoomControlsEnabled(false);
		    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(MISANTLA, 16));
			new GetAllLugares().execute();
		}
	}
	private void setUpMap() {
	    for(Lugar l : lugares_list){
	    	MarkerOptions marker = new MarkerOptions().position(new LatLng(Double.valueOf(l.latitud), Double.valueOf(l.longitud)));
	    	Marker m = mMap.addMarker(marker);
	    	m.setIcon(BitmapDescriptorFactory.fromResource(ICONS[l.categoria]));
	    	list.put(m, l);
	    }
	    mMap.setInfoWindowAdapter(new InfoWindowAdapter() {
			
			@Override
			public View getInfoWindow(Marker marker) {
				return null;
			}
			
			@Override
			public View getInfoContents(Marker marker) {
				View v = getSherlockActivity().getLayoutInflater().inflate(R.layout.map_item_detail, null);
				Lugar l = list.get(marker);
				TextView title = (TextView)v.findViewById(R.id.map_title_item);
				title.setText(l.nombre);
				TextView direccion = (TextView)v.findViewById(R.id.map_direccion_item);
				direccion.setText(l.direccion+"\n"+l.telefono+"\n"+l.email);
				return v;
			}
		});
	    mMap.setOnInfoWindowClickListener(this);
	}

	@Override
	public void onDestroyView() {
	    super.onDestroyView();
	}
	
	class GetAllLugares extends AsyncTask<Void, Void, Boolean>{
		@Override
		protected Boolean doInBackground(Void... params) {
			lugares_list = new ArrayList<Lugar>();
			try{
				URLConnection con = new URL(Estaticos.server_url_all_lugares).openConnection();
				BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));
				JSONArray lugares = new JSONObject(br.readLine()).getJSONArray("lugares");
				Lugar l = null;
				for(int i = 0; i < lugares.length(); i++){
					JSONObject obj = lugares.getJSONObject(i);
					l = new Lugar(obj.getString("nombre"));
					l.descripcion = obj.getString("descripcion");
					l.direccion = obj.getString("direccion");
					l.email = obj.getString("email");
					JSONArray imagenes = obj.getJSONArray("imagenes");
					for(int j = 0; j < imagenes.length(); j++)
						l.addImage(imagenes.getString(j));
					l.latitud = obj.getString("latitud");
					l.longitud = obj.getString("longitud");
					l.telefono = obj.getString("telefono");
					l.categoria = obj.getInt("categoria");
					lugares_list.add(l);
				}
				return true;
			}catch(Exception e){
				e.printStackTrace();
				return false;
			}
		} 
		
		@Override
		protected void onPostExecute(Boolean result) {
			if(result)
				setUpMap();
		}
	}

	@Override
	public void onInfoWindowClick(Marker marker) { 
		Estaticos.lugar = list.get(marker);
		Intent i = new Intent(getSherlockActivity(), DetalleLugar.class);
		startActivity(i);
	}
}
