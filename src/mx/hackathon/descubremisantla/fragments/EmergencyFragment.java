package mx.hackathon.descubremisantla.fragments;

import mx.hackathon.descubremisantla.R;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.actionbarsherlock.app.SherlockFragment;

public class EmergencyFragment extends SherlockFragment implements OnItemClickListener{

	ListView emergencia;
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_emergency, container, false);
        emergencia = (ListView)rootView.findViewById(R.id.lista_emergencia);
        emergencia.setAdapter(new ArrayAdapter<String>(
        		getSherlockActivity(),
        		android.R.layout.simple_list_item_1, 
        		getSherlockActivity().getResources().getStringArray(R.array.emergencia_nombres)));
        emergencia.setOnItemClickListener(this);
        return rootView;
    }
	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
		String telefono = getSherlockActivity().getResources().getStringArray(R.array.emergencia_telefonos)[position];
		String uri = "tel:" + telefono.trim() ;
		Intent intent = new Intent(Intent.ACTION_CALL);
		intent.setData(Uri.parse(uri));
		startActivity(intent);
	}
}
