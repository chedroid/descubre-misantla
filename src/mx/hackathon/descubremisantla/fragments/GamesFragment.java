package mx.hackathon.descubremisantla.fragments;

import java.util.ArrayList;

import com.actionbarsherlock.app.SherlockFragment;

import mx.hackathon.descubremisantla.R;
import mx.hackathon.descubremisantla.utils.CustomImageButton;
import mx.hackathon.descubremisantla.utils.Tablero;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;

public class GamesFragment extends SherlockFragment implements OnClickListener{

	Tablero t;
	int imagenes[] = {
			0, R.drawable.asdasd, R.drawable.asdsad, R.drawable.asdasd, R.drawable.asdasd, R.drawable.asdasd,
			R.drawable.asdsad, R.drawable.asdasd, R.drawable.asdasd
	};
	private static int sw;
	private static int a, b, ii, jj;
	private int WIDTH_SCREEN;
	private int HEIGTH_SCREEN;
	private ArrayList<CustomImageButton> botones;
	GridView grid;
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_games, container, false);
        init(rootView);
        return rootView;
    }
	
	@SuppressLint("NewApi")
	@SuppressWarnings("deprecation")
	public void init(View v){
		t= new Tablero(4);
		t.genAleatorio();
		sw = 0;
		
		grid = (GridView)v.findViewById(R.id.botones);
		
		try{
			Display display = getSherlockActivity().getWindowManager().getDefaultDisplay();
			Point size = new Point();
			display.getSize(size);
			WIDTH_SCREEN = size.x;
			HEIGTH_SCREEN = size.y;
		}catch(Exception e){
			Display display = getSherlockActivity().getWindowManager().getDefaultDisplay(); 
			WIDTH_SCREEN = display.getWidth();   
			HEIGTH_SCREEN = display.getHeight();
		}
		
		grid.setAdapter(new Adapter(getSherlockActivity(), crearBotones()));
	}

	public ArrayList<CustomImageButton> crearBotones (){
		int d = t.getDim();
		this.botones = new ArrayList<CustomImageButton>();
		for (int i = 0; i < d; i++)
		{
			for (int j = 0; j < d; j++)
			{
				CustomImageButton boton = new CustomImageButton(getSherlockActivity(),i,j);
				boton.setOnClickListener(this);
				boton.setScaleType(ImageView.ScaleType.CENTER_CROP);
				boton.setLayoutParams(new GridView.LayoutParams(WIDTH_SCREEN/4, HEIGTH_SCREEN/4));
				botones.add(boton);
			}
		}
		return this.botones;
	}


	public void accion(int x, int y, CustomImageButton boton)
	{
		switch (sw)
		{
		case 0:
			if(!t.esClic(x, y))
			{
				t.clic(x, y);
				boton.setImageResource(imagenes[t.getPos(x,y)]);
				sw = 1;
				a = x;
				b = y;
			}
			break;
		case 1:
			if(!t.esClic(x, y))
			{
				t.clic(x, y);
				boton.setImageResource(imagenes[t.getPos(x,y)]);
				ii = x;
				jj = y;
				if(t.getPos(a, b) != t.getPos(ii, jj))
					sw = 2;
				else
					sw = 0;
			}
			break;
		case 2:
			for(CustomImageButton btn : botones){
				if(btn.getPosI() == a && btn.getPosJ() == b){
					btn.setImageResource(0);
					break;
				}
			}
			for(CustomImageButton b : botones){
				if(b.getPosI() == ii && b.getPosJ() == jj){
					b.setImageResource(0);
					break;
				}
			}
			t.clic(a, b);
			t.clic(ii, jj);
			sw = 0;
			break;
		}
	}
	
	class Adapter extends BaseAdapter{
		ArrayList<CustomImageButton> botones;
		public Adapter(Context context, ArrayList<CustomImageButton> botones){
			this.botones = botones;
		}
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return botones.size();
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return botones.get(arg0);
		}

		@Override
		public View getView(int arg0, View arg1, ViewGroup arg2) {
			return botones.get(arg0);
		}
		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}
		
	}

	@Override
	public void onClick(View v){
		CustomImageButton view = (CustomImageButton)v;
		accion(view.getPosI(),view.getPosJ(), view);
		if(t.esCompleto())
		{
			Toast.makeText(getSherlockActivity(), "Completado", Toast.LENGTH_SHORT).show();
		}		
	}
}