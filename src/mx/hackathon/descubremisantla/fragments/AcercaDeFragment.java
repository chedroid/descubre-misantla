package mx.hackathon.descubremisantla.fragments;

import com.actionbarsherlock.app.SherlockFragment;

import mx.hackathon.descubremisantla.R;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class AcercaDeFragment extends SherlockFragment{

	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_about, container, false);
        return rootView;
    }
}
