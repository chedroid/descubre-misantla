package mx.hackathon.descubremisantla.fragments;


import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import mx.hackathon.descubremisantla.R;
import mx.hackathon.descubremisantla.adapters.EventosAdapter;
import mx.hackathon.descubremisantla.utils.Connection;
import mx.hackathon.descubremisantla.utils.Estaticos;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragment;

public class EventosFragment extends SherlockFragment{

	ListView lista_eventos;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_eventos, null);
		lista_eventos = (ListView)v.findViewById(R.id.events_list);
		if(Connection.checkInternetConnection(getSherlockActivity()))
			new GetEventos().execute();
		else
			Toast.makeText(getSherlockActivity(), "Error al conectar, por favor intenta más tarde", Toast.LENGTH_LONG).show();
		return v;
	}
	public class Evento{
		public String nombre;
		public String fecha;
		public String latitud;
		public String longitud;
		public String descripcion;
		public String imagen;
	}
	
	private ArrayList<Evento> eventos_result;
	class GetEventos extends AsyncTask<Void, Void, Boolean>{
		
		@Override
		protected Boolean doInBackground(Void... params) {
			eventos_result = new ArrayList<EventosFragment.Evento>();
			try{
				URLConnection conn = new URL(Estaticos.server_url_all_eventos).openConnection();
				BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
				JSONArray eventos = new JSONObject(br.readLine()).getJSONArray("eventos");
				Evento e = null;
				for(int i = 0;i < eventos.length(); i++){
					JSONObject aux = eventos.getJSONObject(i);
					e = new Evento();
					e.nombre = aux.getString("nombre");
					e.fecha = aux.getString("fecha");
					e.latitud = aux.getString("latitud");
					e.longitud = aux.getString("longitud");
					e.descripcion = aux.getString("descripcion");
					e.imagen = aux.getString("imagen");
					eventos_result.add(e);
				}
				return true;
			}catch(Exception e){
				e.printStackTrace();
				return false;
			}
		}
		
		@Override
		protected void onPostExecute(Boolean result) {
			if(result)
				lista_eventos.setAdapter(new EventosAdapter(getSherlockActivity(), R.layout.event_item, eventos_result));
		}
	}
}
