package mx.hackathon.descubremisantla.fragments;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import mx.hackathon.descubremisantla.R;
import mx.hackathon.descubremisantla.activities.LugaresActivity;
import mx.hackathon.descubremisantla.utils.Connection;
import mx.hackathon.descubremisantla.utils.Estaticos;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragment;

public class CategoriesFragment extends SherlockFragment implements OnItemClickListener{

	ListView categorias_list;
	public CategoriesFragment() {
	}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.fragments_categories, container, false);
        categorias_list = (ListView)rootView.findViewById(R.id.categories);
        categorias_list.setOnItemClickListener(this);
        if(Connection.checkInternetConnection(getSherlockActivity()))
        	new GetCategories().execute();
        else
        	Toast.makeText(getSherlockActivity(), "Error al conectar, por favor intenta más tarde", Toast.LENGTH_LONG).show();
        return rootView;
    }
	
	class Categoria{
		public String nombre;
		public int id;
		public Categoria(String nombre, int id){
			this.nombre = nombre;
			this.id = id;
		}
	}
	private ArrayList<Categoria> result_categorias;
	
	class GetCategories extends AsyncTask<Void, Void, Boolean>{
		
			@Override
			protected Boolean doInBackground(Void... arg0) {
				result_categorias = new ArrayList<CategoriesFragment.Categoria>();
				try{
					URLConnection conn = new URL(Estaticos.server_url).openConnection();
					BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
					JSONArray categorias = new JSONObject(br.readLine()).getJSONArray("categorias");
					Categoria c = null;
					for(int i = 0; i < categorias.length(); i++){
						JSONObject aux = categorias.getJSONObject(i);
						c = new Categoria(aux.getString("nombre"),aux.getInt("id"));
						result_categorias.add(c);
					}
					return true;
				}catch(Exception e){
					e.printStackTrace();
					return false;
				}
			}
			
			@Override
			protected void onPostExecute(Boolean result) {
				if(result){
					ArrayList<String> to_list = new ArrayList<String>();
					for(Categoria c : result_categorias)
						to_list.add(c.nombre);
					categorias_list.setAdapter(new ArrayAdapter<String>(getSherlockActivity(), android.R.layout.simple_list_item_1, to_list));
				}
			}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
		Intent i = new Intent(getActivity(), LugaresActivity.class);
		i.putExtra("categoria", result_categorias.get(position).id);
		startActivity(i);
	}
}
