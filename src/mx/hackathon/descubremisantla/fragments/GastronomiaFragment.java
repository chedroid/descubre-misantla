package mx.hackathon.descubremisantla.fragments;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import mx.hackathon.descubremisantla.R;
import mx.hackathon.descubremisantla.activities.DetallePlatillo;
import mx.hackathon.descubremisantla.adapters.PlatillosAdapter;
import mx.hackathon.descubremisantla.utils.Connection;
import mx.hackathon.descubremisantla.utils.Estaticos;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragment;

public class GastronomiaFragment extends SherlockFragment implements OnItemClickListener{

	ListView platillos_list;
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_gastronomia, container, false);
        platillos_list = (ListView)rootView.findViewById(R.id.platillos_list);
        platillos_list.setOnItemClickListener(this);
        if(Connection.checkInternetConnection(getSherlockActivity()))
			new GetPlatillos().execute();
		else
			Toast.makeText(getSherlockActivity(), "Error al conectar, por favor intenta más tarde", Toast.LENGTH_LONG).show();
        return rootView;
    }
	
	private ArrayList<Platillo> result_platillos;
	class GetPlatillos extends AsyncTask<Void, Void, Boolean>{
		
		@Override
		protected Boolean doInBackground(Void... arg0) {
			result_platillos = new ArrayList<Platillo>();
			try{
				URLConnection conn = new URL(Estaticos.server_url_platillos).openConnection();
				BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
				JSONArray platillos = new JSONObject(br.readLine()).getJSONArray("platillos");
				Platillo p = null;
				for(int i = 0; i < platillos.length(); i++){
					JSONObject aux = platillos.getJSONObject(i);
					p = new Platillo(aux.getString("nombre"), aux.getString("ingredientes"), aux.getString("procedimiento"));
					JSONArray images = aux.getJSONArray("imagenes");
					for(int j = 0 ;j < images.length(); j++)
						p.addImage(images.getString(j));
					result_platillos.add(p); 
				}
				return true;
			}catch(Exception e){
				e.printStackTrace();
				return false; 
			}
		}
		
		@Override
		protected void onPostExecute(Boolean result) {
			if(result)
				platillos_list.setAdapter(new PlatillosAdapter(getSherlockActivity(), R.layout.platillo_item, result_platillos));
		}
}

	public class Platillo{
		public String nombre;
		public String ingredientes;
		public String procedimiento;
		public ArrayList<String> imagenes;
		public Platillo(String nombre, String ingrentes, String procedimiento){
			this.nombre = nombre;
			this.ingredientes = ingrentes;
			this.procedimiento = procedimiento;
			this.imagenes = new ArrayList<String>();
		}
		public void addImage(String image){
			this.imagenes.add(image);
		}
	}
	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
		Estaticos.platillo = result_platillos.get(position);
		Intent i = new Intent(getActivity(), DetallePlatillo.class);
		startActivity(i);
	}
}
