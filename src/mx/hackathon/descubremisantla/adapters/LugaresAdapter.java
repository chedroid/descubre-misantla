package mx.hackathon.descubremisantla.adapters;

import java.util.ArrayList;

import com.loopj.android.image.SmartImageView;

import mx.hackathon.descubremisantla.R;
import mx.hackathon.descubremisantla.activities.LugaresActivity.Lugar;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class LugaresAdapter extends ArrayAdapter<Lugar>{
	
	private ArrayList<Lugar> lugares_list;
	
	public LugaresAdapter(Context context, int resId, ArrayList<Lugar> objects) {
		super(context, resId, objects);
		this.lugares_list = objects;
	}
	
	@Override
	public int getCount() {
		return lugares_list.size();
	}
	
	static class ViewHolder{
		public static TextView nombre;
		public static SmartImageView imagen;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if(convertView == null){
			LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.lugares_item, null);
			ViewHolder.nombre = (TextView)convertView.findViewById(R.id.nombre_lugar);
			ViewHolder.imagen = (SmartImageView)convertView.findViewById(R.id.imagen_lugar);
		}
		Lugar l = getItem(position);
		ViewHolder.nombre.setText(l.nombre);
		if(l.imagenes.size() > 0)
			ViewHolder.imagen.setImageUrl(l.imagenes.get(0));
		return convertView;
	}
}
