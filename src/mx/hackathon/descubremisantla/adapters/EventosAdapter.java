package mx.hackathon.descubremisantla.adapters;

import java.util.ArrayList;

import mx.hackathon.descubremisantla.R;
import mx.hackathon.descubremisantla.fragments.EventosFragment.Evento;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class EventosAdapter extends ArrayAdapter<Evento>{

	private ArrayList<Evento> eventos;
	public EventosAdapter(Context context, int resid, ArrayList<Evento> eventos){
		super(context, resid, eventos);
		this.eventos = eventos;
	}
	
	public int getCount() {
		return this.eventos.size();
	};
	
	static class Viewholder{
		public static TextView title;
		public static TextView date;
		public static TextView descripcion;
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if(convertView ==  null){
			LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.event_item,null);
			Viewholder.title = (TextView)convertView.findViewById(R.id.event_title);
			Viewholder.date = (TextView)convertView.findViewById(R.id.event_date);
			Viewholder.descripcion = (TextView)convertView.findViewById(R.id.event_detail);
		}
		Evento e = getItem(position);
		Viewholder.title.setText(e.nombre);
		Viewholder.date.setText(e.fecha);
		Viewholder.descripcion.setText(e.descripcion);
		return convertView;
	}
}
