package mx.hackathon.descubremisantla.adapters;

import java.util.ArrayList;

import com.loopj.android.image.SmartImageView;

import mx.hackathon.descubremisantla.R;
import mx.hackathon.descubremisantla.fragments.GastronomiaFragment.Platillo;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class PlatillosAdapter extends ArrayAdapter<Platillo>{

	private ArrayList<Platillo> platillos;
	public PlatillosAdapter(Context context, int resId, ArrayList<Platillo> platillos){
		super(context, resId, platillos);
		this.platillos = platillos;
	}
	
	@Override
	public int getCount() {
		return this.platillos.size();
	}
	
	static class ViewHolder{
		public static SmartImageView image;
		public static TextView nombre;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if(convertView == null){
			LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.platillo_item, null);
			ViewHolder.image = (SmartImageView)convertView.findViewById(R.id.imagen_platillo);
			ViewHolder.nombre = (TextView)convertView.findViewById(R.id.nombre_platillo);
		}
		Platillo p = getItem(position);
		if(p.imagenes.size() > 0)
			ViewHolder.image.setImageUrl(p.imagenes.get(0));
		ViewHolder.nombre.setText(p.nombre);
		return convertView;
	}
}
