package mx.hackathon.descubremisantla.utils;

import mx.hackathon.descubremisantla.activities.LugaresActivity.Lugar;
import mx.hackathon.descubremisantla.fragments.GastronomiaFragment.Platillo;

public class Estaticos {
	public static final String server_url = "http://192.168.43.223:8000/misantla/categorias";
	public static final String server_url_lugares = "http://192.168.43.223:8000/misantla/lugares/";
	public static final String server_url_platillos = "http://misantla.dragonflylabs.com.mx/home/platillos/";
	public static final String server_url_all_lugares = "http://misantla.dragonflylabs.com.mx/home/all_lugares/";
	public static final String server_url_all_eventos = "http://192.168.43.223:8000/misantla/eventos/";
	public static final String server_url_register = "http://misantla.dragonflylabs.com.mx/home/registro/";
	public static final String server_url_login = "http://misantla.dragonflylabs.com.mx/home/login/";
	public static final String server_url_logout = "http://misantla.dragonflylabs.com.mx/home/logout/";
	public static Lugar lugar;
	public static Platillo platillo;
    public static final String PREF_ACCESS_TOKEN = "accessToken";
}
