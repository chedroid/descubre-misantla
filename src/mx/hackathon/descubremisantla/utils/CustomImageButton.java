package mx.hackathon.descubremisantla.utils;

import android.content.Context;
import android.widget.ImageButton;

public class CustomImageButton extends ImageButton{

	int i,j;
	public CustomImageButton(Context context, int i, int j) {
		super(context);
		this.i = i;
		this.j = j;
	}
	
	public int getPosI(){ return i;}
	public int getPosJ(){ return j;}

	
}
