package mx.hackathon.descubremisantla.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class Connection {
	public static boolean checkInternetConnection(Context context){
		ConnectivityManager conexion= (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo net=conexion.getActiveNetworkInfo();
        if(net!=null&&net.getState()== NetworkInfo.State.CONNECTED)
            return true;
        return false;
	}
}
