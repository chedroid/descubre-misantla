package mx.hackathon.descubremisantla.activities;

import java.io.Serializable;
import java.net.URI;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import mx.hackathon.descubremisantla.R;
import mx.hackathon.descubremisantla.adapters.LugaresAdapter;
import mx.hackathon.descubremisantla.utils.Connection;
import mx.hackathon.descubremisantla.utils.Estaticos;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;

public class LugaresActivity extends SherlockActivity implements OnItemClickListener{
	
	ListView lugares_list;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_lugares);
		getSupportActionBar().setTitle("Categorías");
		lugares_list = (ListView)findViewById(R.id.lugares_list);
		lugares_list.setOnItemClickListener(this);
		if(Connection.checkInternetConnection(this))
			new GetLugares().execute(getIntent().getIntExtra(("categoria"),0));
		else
			Toast.makeText(this, "Error al conectar, por favor intenta más tarde", Toast.LENGTH_LONG).show();
	}
	
	private ArrayList<Lugar> result_lugares;
	class GetLugares extends AsyncTask<Integer, Void, Boolean>{
		@Override
		protected Boolean doInBackground(Integer... params) {
			result_lugares = new ArrayList<LugaresActivity.Lugar>();
			try{
				URI uri=new URI(Estaticos.server_url_lugares);
	            HttpResponse response=null;
		        ArrayList<BasicNameValuePair> parametros= new ArrayList<BasicNameValuePair>();
		        BasicNameValuePair parametro1=new BasicNameValuePair("categoria", String.valueOf(params[0]));
		        parametros.add(parametro1);
		        HttpPost post=new HttpPost(uri);
		        UrlEncodedFormEntity entity=new UrlEncodedFormEntity(parametros,HTTP.UTF_8);
		        post.setEntity(entity);
		        HttpClient client= new DefaultHttpClient();
		        response=client.execute(post);
		        HttpEntity e = response.getEntity();
		        String responseString = EntityUtils.toString(e, "UTF-8");
		        JSONArray lugares = new JSONObject(responseString).getJSONArray("lugares");
		        Lugar l = null;
		        for(int i = 0; i < lugares.length(); i++){
		        	JSONObject aux = lugares.getJSONObject(i);
		        	l = new Lugar(aux.getString("nombre"));
		        	l.descripcion = aux.getString("descripcion");
		        	l.direccion = aux.getString("direccion");
		        	l.email = aux.getString("email");
		        	l.latitud = aux.getString("latitud");
		        	l.longitud = aux.getString("longitud");
		        	l.telefono = aux.getString("telefono");
		        	JSONArray images = aux.getJSONArray("imagenes");
		        	for(int j = 0; j < images.length(); j++)
		        		l.addImage(images.getString(j));
		        	result_lugares.add(l);
		        }
		        return true;
			}catch(Exception e){
				e.printStackTrace();
				return false;
			}
		}
		@Override
		protected void onPostExecute(Boolean result) {
			if(result)
				if(result_lugares.size() > 0)
					lugares_list.setAdapter(new LugaresAdapter(getBaseContext(), R.layout.lugares_item, result_lugares));
				else{
					Toast.makeText(getApplication(), "No hay lugares en esta categoría", Toast.LENGTH_LONG).show();
					finish();
				}
		}
	}
	
	public static class Lugar implements Serializable{
		private static final long serialVersionUID = 1L;
		public String nombre;
		public String descripcion;
		public String direccion;
		public String telefono;
		public String email;
		public String latitud;
		public String longitud;
		public int categoria;
		public ArrayList<String> imagenes;
		public Lugar(String nombre){
			this.nombre = nombre;
			imagenes = new ArrayList<String>();
		}
		public void addImage(String image){
			imagenes.add(image);
		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
		Estaticos.lugar = result_lugares.get(position);
		Intent i = new Intent(this, DetalleLugar.class);
		startActivity(i);
	}
}
