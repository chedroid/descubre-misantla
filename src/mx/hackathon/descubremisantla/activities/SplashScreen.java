package mx.hackathon.descubremisantla.activities;

import mx.hackathon.descubremisantla.R;
import mx.hackathon.descubremisantla.utils.Estaticos;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MotionEvent;

import com.actionbarsherlock.app.SherlockActivity;

public class SplashScreen extends SherlockActivity{

	private SharedPreferences mPrefs;
	protected boolean _active = true;
    protected int _splashTime = 3000;
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        mPrefs = getSharedPreferences("misantlaPrefs", MODE_PRIVATE);
        setContentView(R.layout.activity_splash);
         
        Thread splashTread = new Thread() {
            @Override 
            public void run() {
                try {
                    int waited = 0;
                    while(_active && (waited < _splashTime)) {
                        sleep(100);
                        if(_active) {
                            waited += 100;
                        }
                    }
                } catch(InterruptedException e) {
                	
                } finally {
                	this.interrupt();
                    if (mPrefs.contains(Estaticos.PREF_ACCESS_TOKEN) && !TextUtils.isEmpty(mPrefs.getString(Estaticos.PREF_ACCESS_TOKEN, null))){
                    	Intent i = new Intent(getBaseContext(), MainActivity.class);
                    	startActivity(i);
                    }else{
                    	Intent i = new Intent(getBaseContext(), WalkthroughActivity.class);
                    	startActivity(i);
                    }
                    finish();
                }
            }
        };
        splashTread.start();
    } 
     
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            _active = false;
        }
        return true;
    }
}
