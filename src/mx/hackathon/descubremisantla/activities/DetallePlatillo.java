package mx.hackathon.descubremisantla.activities;

import mx.hackathon.descubremisantla.R;
import mx.hackathon.descubremisantla.fragments.GastronomiaFragment.Platillo;
import mx.hackathon.descubremisantla.utils.Estaticos;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.loopj.android.image.SmartImageView;

public class DetallePlatillo extends SherlockActivity{

	private TextView ingredientes;
	private TextView procedimiento;
	private SmartImageView imagen;
	private Platillo p;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_detail_platillo);
		p = Estaticos.platillo;
		getSupportActionBar().setTitle(p.nombre);
		ingredientes = (TextView)findViewById(R.id.detail_ingredientes);
		procedimiento = (TextView)findViewById(R.id.detail_procedimiento);
		imagen = (SmartImageView)findViewById(R.id.detall_imagen_platillo);
		ingredientes.setText(p.ingredientes);
		procedimiento.setText(p.procedimiento);
		if(p.imagenes.size() > 0)
			imagen.setImageUrl(p.imagenes.get(0));
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getSupportMenuInflater().inflate(R.menu.details, menu);
		return true;
	}

	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.share:
			Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND); 
		    sharingIntent.setType("text/plain");
		    String shareBody = "Acabo de ver como preparar "+p.nombre+" en Descubre Misantla!";
		    if(p.imagenes.size() > 0)
		    	shareBody = "Acabo de ver como preparar "+p.nombre+" en Descubre Misantla! "+p.imagenes.get(0);
		    sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, p.nombre);
		    sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
		    startActivity(Intent.createChooser(sharingIntent, "Compartir vía"));
			return true;
		case android.R.id.home:
			onBackPressed();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
}
