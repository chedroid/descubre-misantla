package mx.hackathon.descubremisantla.activities;

import mx.hackathon.descubremisantla.R;
import mx.hackathon.descubremisantla.api.Server;
import mx.hackathon.descubremisantla.utils.Connection;
import mx.hackathon.descubremisantla.utils.Estaticos;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;

public class Login extends SherlockActivity implements OnClickListener{

	private Button login;
	private EditText username;
	private EditText password;
	private Context context = this;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		login = (Button)findViewById(R.id.login_login);
		username = (EditText)findViewById(R.id.login_username);
		password = (EditText)findViewById(R.id.login_password);
		login.setOnClickListener(this);
	}
	
	class LoginTask extends AsyncTask<Void, Void, Boolean>{
		private ProgressDialog dialog;
		private Server server;
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog = new ProgressDialog(context);
			dialog.setTitle("Por favor espere...");
			dialog.setMessage("Iniciando sesión");
			dialog.show();
			server = new Server();
		}
		
		@Override
		protected Boolean doInBackground(Void... params) {
			if(server.login(username.getText().toString(), password.getText().toString())){
				SharedPreferences.Editor editor = getSharedPreferences("misantlaPrefs", MODE_PRIVATE).edit();
				editor.putString(Estaticos.PREF_ACCESS_TOKEN, server.getServerToken()).commit();
				return true;
			}
			return false;
		}
		
		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			Toast.makeText(context, server.getServerMessage(), Toast.LENGTH_LONG).show();
			dialog.dismiss();
			if(result){
				Intent i = new Intent(context, MainActivity.class);
				startActivity(i);
				finish();
			}
		}
	}
	@Override
	public void onClick(View v) {
		if(Connection.checkInternetConnection(context))
			new LoginTask().execute();
		else
			Toast.makeText(context, "No hay conexión a internet", Toast.LENGTH_SHORT).show();
	}
}
