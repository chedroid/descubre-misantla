package mx.hackathon.descubremisantla.activities;

import java.util.HashMap;

import mx.hackathon.descubremisantla.R;
import mx.hackathon.descubremisantla.api.Server;
import mx.hackathon.descubremisantla.utils.Estaticos;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;

public class Registro extends SherlockActivity implements OnClickListener{

	public static final String REGISTRO_NOMBRE = "nombre";
	public static final String REGISTRO_USUARIO = "username";
	public static final String REGISTRO_APPAT = "appat";
	public static final String REGISTRO_APMAT = "apmat";
	public static final String REGISTRO_PASSWORD = "password";
	public static final String REGISTRO_SEXO = "sexo";
	public static final String REGISTRO_EXITOSO = "registro";
	private Context context = this;
	private EditText username;
	private EditText password;
	private EditText nombre;
	private EditText appat;
	private EditText apmat;
	private Spinner sexo;
	private Button registro;
	private SharedPreferences prefs;
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_registro);
		getSupportActionBar().setTitle(getResources().getString(R.string.register));
		prefs = getSharedPreferences("misantlaPrefs", MODE_PRIVATE);
		username = (EditText)findViewById(R.id.registro_correo);
		password = (EditText)findViewById(R.id.registro_password);
		nombre = (EditText)findViewById(R.id.registro_nombre);
		appat = (EditText)findViewById(R.id.registro_apaterno);
		apmat = (EditText)findViewById(R.id.registro_amaterno);
		sexo = (Spinner)findViewById(R.id.registro_sexo);
		registro = (Button)findViewById(R.id.registro_registro_button);
		registro.setOnClickListener(this);
	}
	
	class GetToken extends AsyncTask<Void, Void, Boolean>{
		private ProgressDialog dialog;
		private Server server;
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog = new ProgressDialog(context);
			dialog.setTitle("Por favor espere...");
			dialog.setMessage("Solicitando registro");
			dialog.show();
			server = new Server();
		}
		
		@Override
		protected Boolean doInBackground(Void... arg0) {
			HashMap<String, String> values = new HashMap<String, String>();
			values.put(REGISTRO_NOMBRE, nombre.getText().toString());
			values.put(REGISTRO_APPAT, appat.getText().toString());
			values.put(REGISTRO_APMAT, apmat.getText().toString());
			values.put(REGISTRO_USUARIO, username.getText().toString());
			values.put(REGISTRO_PASSWORD, password.getText().toString());
			values.put(REGISTRO_SEXO, (sexo.getSelectedItemPosition() == 0) ? "M" : "F");
			return server.get_register_token(values);
		}
		
		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			dialog.dismiss();
			if(result){
				SharedPreferences.Editor editor = prefs.edit();
				editor.putString(Estaticos.PREF_ACCESS_TOKEN, server.getServerToken());
				editor.commit();
				Toast.makeText(context, server.getServerMessage(), Toast.LENGTH_LONG).show();
				Intent i = getIntent();
				i.putExtra(REGISTRO_EXITOSO, result);
				setResult(RESULT_OK, i);
				finish();
				Log.d("Mensaje", "entre");
			}
			Toast.makeText(context, server.getServerMessage(), Toast.LENGTH_LONG).show();
		}
	}

	@Override
	public void onClick(View v) {
		new GetToken().execute();
	}
} 