package mx.hackathon.descubremisantla.activities;

import mx.hackathon.descubremisantla.R;
import mx.hackathon.descubremisantla.api.Server;
import mx.hackathon.descubremisantla.utils.Connection;
import mx.hackathon.descubremisantla.utils.Estaticos;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockPreferenceActivity;

public class Configuracion extends SherlockPreferenceActivity{

	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.preferences);
		findPreference("logout").setOnPreferenceClickListener(new OnPreferenceClickListener() {
			
			@Override
			public boolean onPreferenceClick(Preference arg0) {
				if(Connection.checkInternetConnection(getBaseContext()))
					new Logout().execute();
				else
					Toast.makeText(getBaseContext(), "No hay conexión a internet", Toast.LENGTH_SHORT).show();
				return true;
			}
		});
	}
	
	class Logout extends AsyncTask<Void, Void, Boolean>{
		private Server server;
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			server = new Server();
		}
		@Override
		protected Boolean doInBackground(Void... params) {
			SharedPreferences prefs = getSharedPreferences("misantlaPrefs", MODE_PRIVATE);
			String token = prefs.getString(Estaticos.PREF_ACCESS_TOKEN, "");
			if(server.logout(token)){
				SharedPreferences.Editor editor = prefs.edit();
				editor.remove(Estaticos.PREF_ACCESS_TOKEN).commit();
				return true;
			}
			return false;
		}
		
		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			Toast.makeText(getApplicationContext(), server.getServerMessage(), Toast.LENGTH_LONG).show();
			if(result){
				Intent i = new Intent(getApplicationContext(), WalkthroughActivity.class);
				i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(i);
				finish();
			}
		}
	}
}
