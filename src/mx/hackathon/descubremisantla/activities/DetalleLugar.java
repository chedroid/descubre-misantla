package mx.hackathon.descubremisantla.activities;

import mx.hackathon.descubremisantla.R;
import mx.hackathon.descubremisantla.activities.LugaresActivity.Lugar;
import mx.hackathon.descubremisantla.utils.Estaticos;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.loopj.android.image.SmartImageView;

public class DetalleLugar extends SherlockActivity{
	
	private TextView direccion;
	private TextView descripcion;
	private SmartImageView imagen;
	private Lugar l;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_detail_lugar);
		l = Estaticos.lugar;
		getSupportActionBar().setTitle(l.nombre);
		direccion = (TextView)findViewById(R.id.detail_direccion);
		descripcion = (TextView)findViewById(R.id.detail_descripcion);
		direccion.setText(l.direccion);
		descripcion.setText(l.descripcion);
		imagen = (SmartImageView)findViewById(R.id.detall_imagen_lugar);
		if(l.imagenes.size() > 0)
			imagen.setImageUrl(l.imagenes.get(0));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getSupportMenuInflater().inflate(R.menu.details, menu);
		return true;
	}

	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.share:
			Intent share = new Intent();
			share.setAction(Intent.ACTION_SEND);
			share.putExtra(Intent.EXTRA_TEXT, "Acabo de visitar: "+l.nombre+" a través de Descubre Misantla!");
			startActivity(share);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
}
