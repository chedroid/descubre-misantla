package mx.hackathon.descubremisantla.activities;


import com.actionbarsherlock.app.SherlockActivity;
import com.viewpagerindicator.CirclePageIndicator;

import mx.hackathon.descubremisantla.R;
import mx.hackathon.descubremisantla.adapters.ViewAdapter;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.VideoView;


public class WalkthroughActivity extends SherlockActivity implements OnClickListener{

	public static final int RESULT_REGISTRO = 1;
	public static final int RESULT_LOGIN = 0;
	ViewPager viewPager = null;
	Button login, register;
	CirclePageIndicator mIndicator;
	Handler handler;
	VideoView video;
	Context context = this;
	
	private Runnable increment = new Runnable() {
		@Override
		public void run() {
			int currentItem = viewPager.getCurrentItem();
		      int maxItems = viewPager.getAdapter().getCount();
		      if (maxItems != 0)
		        viewPager.setCurrentItem((currentItem + 1) % maxItems, true);
		      else
		        viewPager.setCurrentItem(0, true);
		      handler.postDelayed(increment, 6000);
		}
	};
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		getSupportActionBar().hide();
		setContentView(R.layout.activity_walktrought);
		getWindow().setFormat(PixelFormat.TRANSLUCENT);
		video = (VideoView)findViewById(R.id.tutorial_video);
		Uri videoUrl = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.intro);
		video.setVideoURI(videoUrl);
		video.setOnCompletionListener(new OnCompletionListener() {
			
			@Override
			public void onCompletion(MediaPlayer arg0) {
				video.start();
			}
		});
		viewPager = (ViewPager)findViewById(R.id.viewPager);
		viewPager.setAdapter(new ViewAdapter(this));
        mIndicator = (CirclePageIndicator)findViewById(R.id.indicator);
        mIndicator.setViewPager(viewPager);
        handler = new Handler();
        login = (Button)findViewById(R.id.login_button);
        login.setOnClickListener(this);
        register = (Button)findViewById(R.id.register_button);
        register.setOnClickListener(this);
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		handler.removeCallbacks(increment);
        handler.postDelayed(increment, 4000);
        video.start();
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		handler.removeCallbacks(increment);
		video.stopPlayback();
	}

	@Override
	public void onClick(View v) {
		if(v.getId() == R.id.login_button){
			Intent i = new Intent(this, Login.class);
			startActivityForResult(i, RESULT_LOGIN);
		}else if(v.getId() == R.id.register_button){
			Intent i = new Intent(this, Registro.class);
			startActivityForResult(i, RESULT_REGISTRO);
		}
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(resultCode == RESULT_OK){
			boolean status = data.getBooleanExtra(Registro.REGISTRO_EXITOSO, false);
			if(status){
				Intent i = new Intent(context, MainActivity.class);
				startActivity(i);
				finish();
			}
		}
	}
}