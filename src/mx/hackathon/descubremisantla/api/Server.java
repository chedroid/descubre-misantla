package mx.hackathon.descubremisantla.api;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;

import mx.hackathon.descubremisantla.activities.Registro;
import mx.hackathon.descubremisantla.utils.Estaticos;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

public class Server {

	private String msg;
	private String token;
	public String getServerMessage(){
		return msg;
	}
	
	public String getServerToken(){
		return token;
	}
	public boolean get_register_token(HashMap<String, String> values){
		try{
			URI uri=new URI(Estaticos.server_url_register);
            HttpResponse response=null;
	        ArrayList<BasicNameValuePair> parametros= new ArrayList<BasicNameValuePair>();
	        BasicNameValuePair parametro1=new BasicNameValuePair(Registro.REGISTRO_NOMBRE,values.get(Registro.REGISTRO_NOMBRE));
	        BasicNameValuePair parametro2=new BasicNameValuePair(Registro.REGISTRO_APPAT,values.get(Registro.REGISTRO_APPAT));
	        BasicNameValuePair parametro3=new BasicNameValuePair(Registro.REGISTRO_APMAT,values.get(Registro.REGISTRO_APMAT));
	        BasicNameValuePair parametro4=new BasicNameValuePair(Registro.REGISTRO_USUARIO,values.get(Registro.REGISTRO_USUARIO));
	        BasicNameValuePair parametro5=new BasicNameValuePair(Registro.REGISTRO_PASSWORD,values.get(Registro.REGISTRO_PASSWORD));
	        BasicNameValuePair parametro6=new BasicNameValuePair(Registro.REGISTRO_SEXO,values.get(Registro.REGISTRO_SEXO));
	        parametros.add(parametro1);
	        parametros.add(parametro2);
	        parametros.add(parametro3);
	        parametros.add(parametro4);
	        parametros.add(parametro5);
	        parametros.add(parametro6);
	         
	        HttpPost post=new HttpPost(uri);
	        UrlEncodedFormEntity entity=new UrlEncodedFormEntity(parametros,HTTP.UTF_8);
	        post.setEntity(entity);
	         
	        HttpClient client = createHttpClient();
	        response=client.execute(post);
	        HttpEntity e = response.getEntity();
	        String responseString = EntityUtils.toString(e, "UTF-8");
	        JSONObject json = new JSONObject(responseString);
	        msg = json.getString("msg");
	        boolean status = json.getBoolean("status");
	        if(status){
	        	token = json.getString("token");
	        	return true;
	        }else{
	        	return false;
	        }
		}catch(Exception e){
			e.printStackTrace();
			msg = "Error interno, discúlpanos por favor";
			return false;
		}
	}
	
	public boolean login(String username, String password){
		try{
			URI uri=new URI(Estaticos.server_url_login);
            HttpResponse response=null;
	        ArrayList<BasicNameValuePair> parametros= new ArrayList<BasicNameValuePair>();
	        BasicNameValuePair parametro1=new BasicNameValuePair(Registro.REGISTRO_USUARIO,username);
	        BasicNameValuePair parametro2=new BasicNameValuePair(Registro.REGISTRO_PASSWORD,password);
	        parametros.add(parametro1);
	        parametros.add(parametro2);
	        HttpPost post=new HttpPost(uri);
	        UrlEncodedFormEntity entity=new UrlEncodedFormEntity(parametros,HTTP.UTF_8);
	        post.setEntity(entity);
	        HttpClient client = createHttpClient();
	        response=client.execute(post);
	        HttpEntity e = response.getEntity();
	        String responseString = EntityUtils.toString(e, "UTF-8");
	        JSONObject json = new JSONObject(responseString);
	        msg = json.getString("msg");
	        token = json.getString("token");
	        return json.getBoolean("status");
		}catch(Exception e){
			e.printStackTrace();
			msg = "Error interno, discúlpanos por favor";
			return false;
		}
	}
	
	public boolean logout(String token){
		try{
			URI uri=new URI(Estaticos.server_url_logout);
            HttpResponse response=null;
	        ArrayList<BasicNameValuePair> parametros= new ArrayList<BasicNameValuePair>();
	        BasicNameValuePair parametro1=new BasicNameValuePair("token",token);
	        parametros.add(parametro1);
	        HttpPost post=new HttpPost(uri);
	        UrlEncodedFormEntity entity=new UrlEncodedFormEntity(parametros,HTTP.UTF_8);
	        post.setEntity(entity);
	        HttpClient client = createHttpClient();
	        response=client.execute(post);
	        HttpEntity e = response.getEntity();
	        String responseString = EntityUtils.toString(e, "UTF-8");
	        JSONObject json = new JSONObject(responseString);
	        msg = json.getString("msg");
	        return json.getBoolean("status");
		}catch(Exception e){
			e.printStackTrace();
			msg = "Error interno, discúlpanos por favor";
			return false;
		}
	}
	
	private HttpClient createHttpClient()
	{
	    HttpParams params = new BasicHttpParams();
	    HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
	    HttpProtocolParams.setContentCharset(params, HTTP.DEFAULT_CONTENT_CHARSET);
	    HttpProtocolParams.setUseExpectContinue(params, true);

	    SchemeRegistry schReg = new SchemeRegistry();
	    schReg.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
	    schReg.register(new Scheme("https", SSLSocketFactory.getSocketFactory(), 443));
	    ClientConnectionManager conMgr = new ThreadSafeClientConnManager(params, schReg);

	    return new DefaultHttpClient(conMgr, params);
	}
}
