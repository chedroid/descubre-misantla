package mx.hackathon.descubremisantla.gcm;

public class MagicValues {

    /**
     * Substitute you own sender ID here. This is the project number you got
     * from the API Console, as described in "Getting Started."
     */
    public static final String SENDER_ID = "269831761485";
    
    //Your server name and port
    public static String server = "http://192.168.43.223:8000";
    
	public static String JSON_KEY_REASON = "reason";
	public static String JSON_KEY_STATUS = "status";
	public static String JSON_SUCCESS = "success";
	public static String JSON_FAIL = "fail";
}
